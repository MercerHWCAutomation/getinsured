/**
 * Created by subhajit-chakraborty on 7/6/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    TestingEnvironment: 'PROD',

    FooterPara1: "Mercer Marketplace is provided by Mercer Health & Benefits Administration LLC. This website is owned by Mercer Health & Benefits Administration LLC. The Centers for Medicare & Medicaid Services has not endorsed the information contained in this website.",
    FooterPara2: "Your employer subsidy contribution listed on this website reflects your 2018 employer subsidy contribution at the beginning of the plan year and may not reflect your current balance. Most recent employer subsidy contributions, balance, and activity can be viewed by clicking on the Subsidy Account link at the top of the page. You will then need to input your Subsidy Account Username and Password. Please refer to your employer subsidy plan document for a complete description of eligibility and benefits.",

   //Non Gated Data for Exchange flows:
    Zipcode: 60601,
    Month: 10,
    Day: 07,
    Year: 1965,
    HouseholdIncomeValue: 40000,
    CopyrightYear: '2017',


    Microwait: '3000',
    shortWait: 5000,
    longWait: 10000,
    LoginWait: 30000,

    WrongFirstName: 'WrongFirstName',
    WrongLastName: 'WrongLastName',
    WrongSSN: '0000',
    EndDateOE: '12/15/2017',

    MedicareURL: 'https://mercermarketplace.destinationrx.com/PlanCompare/Consumer/Type3/2018/Compare/PinHome',
    DentalURL: 'https://www.yourdentalexchange.com/di/web/yourdentalexchange/',
    VisionURL: 'https://www.yourdentalexchange.com/di/web/yourdentalexchange/',
    OtherInsuranceURL: 'http://retiree.mercermarketplace.com/other-insurance.html',

    WrongZipcode: '02',
    WrongMonthSpouse: '13',
    WrongDaySpouse: '35',
    WrongYearSpouse: '2015',

    Child1Month: '12',
    Child1Day: '12',
    Child1Year: '1999',

    ContributionParagraph: 'Your employer is providing a monthly subsidy to offset your healthcare expenses, which may include monthly premiums and other eligible out-of-pocket expenses. The subsidy amounts shown do not reflect any amount tied to a Medicare-eligible participant.',
    HRADisclaimer: 'HRA DISCLAIMER: Your employer subsidy contribution listed on this site reflects your 2018 employer subsidy at the beginning of the plan year and may not reflect your current balance. Your employer subsidy balances may not be displayed for dependent children who have been manually edited on the household page of this site. Current employer subsidy balance and activity can be viewed by clicking on the Subsidy Account link at the top of the page. Please refer to your employer subsidy plan document for a complete description of eligibility and benefits.',
    TaxCreditDisclaimer: 'TAX CREDIT DISCLAIMER: This tool provides a quick view of qualifications for different savings programs in your state. You’ll find out exactly what you qualify for only when you fill out a Marketplace application and get your eligibility results from the Marketplace. Please visit HealthCare.gov for more information..',
    OffExchangeURL: 'exchangeType=OFF',

    DoctorsName: 'WILL',
    DrugName: 'primsol',





}
