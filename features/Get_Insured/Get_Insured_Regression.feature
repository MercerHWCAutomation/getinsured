Feature: Get Insured Regression Test cases

  @Regression @VerifyFooterPrivacyLinks
  Scenario Outline: To Verify footer and privacy links for gated and ungated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    And  User should able to verify the footer Paragraphs
    When  User should able to verify terms and Condition and Privacy Policy link

    Examples:
    #Gated Portal
      | GetInsuredURL                                                |
      |https://RetireeHealth.premedicareplans.com/subsidy            |
   #   |https://Ahlstrom.premedicareplans.com/subsidy                 |
   #   |https://Enbridge.premedicareplans.com/subsidy                 |
   #   |httsp://Momentive.premedicareplans.com/subsidy                |
   #   |https://Motorolasolutions.premedicareplans.com/subsidy        |
   #   |https://TransCanada.premedicareplans.com/subsidy              |
   #   |https://ComcastNBCU.premedicareplans.com/subsidy              |
#
    #  #Ungated Portal
    #  |https://RetireeHealthAccess.premedicareplans.com/accessonly   |
    #  |https://AECOM.premedicareplans.com/accessonly                 |
    #  |https://Albemarle.premedicareplans.com/accessonly             |
    #  |https://Devon.premedicareplans.com/accessonly                 |
    #  |https://Retiree.premedicareplans.com/accessonly               |
    #  |https://Quest.premedicareplans.com/accessonly                 |
    #  |https://Silgan.premedicareplans.com/accessonly                |
    #  |https://TCaccess.premedicareplans.com/accessonly              |
    #  |https://URS.premedicareplans.com/accessonly                   |
    #  |https://MSIaccess.premedicareplans.com/accessonly             |
    #  |https://ComcastNBCUAccess.premedicareplans.com/accessonly     |
    #  |https://OfficeDepot.premedicareplans.com/accessonly           |

  @Regression @VerifyHomePage
  Scenario Outline: To Verify homepage for gated and ungated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    And User able to verify the tollfree number "<TollFreeNumber>"
    And  User able to verify the logo of Mercer Marketplace
    And  User able to verify the subsidy account link
    And  User able to verify home link header
    And  User able to verify Helpful link
    And  User should able to verify the login page of "<GetInsuredURL>" Portals
    Then  User able to verify the copyright year

    Examples:
    #Gated Portal
      | GetInsuredURL                                                |TollFreeNumber |
      |https://RetireeHealth.premedicareplans.com/subsidy            |866-609-4810   |
      |https://Ahlstrom.premedicareplans.com/subsidy                 |844-618-6285   |
      |https://Enbridge.premedicareplans.com/subsidy                 |888-264-9246   |
      |https://Momentive.premedicareplans.com/subsidy                |888-264-9323   |
      |https://Motorolasolutions.premedicareplans.com/subsidy        |844-851-5426   |
      |https://TransCanada.premedicareplans.com/subsidy              |844-618-6289   |
      |https://ComcastNBCU.premedicareplans.com/subsidy              |866-435-5135   |
#
      #Ungated Portal
      |https://RetireeHealthAccess.premedicareplans.com/accessonly   |866-696-8685   |
      |https://AECOM.premedicareplans.com/accessonly                 |877-975-3275   |
      |https://Albemarle.premedicareplans.com/accessonly             |866-658-9898   |
      |https://Devon.premedicareplans.com/accessonly                 |844-213-9959   |
      |https://Retiree.premedicareplans.com/accessonly               |800-752-7049   |
      |https://Quest.premedicareplans.com/accessonly                 |888-264-9876   |
      |https://Silgan.premedicareplans.com/accessonly                |800-685-6350   |
      |https://TCaccess.premedicareplans.com/accessonly              |866-609-4803   |
      |https://URS.premedicareplans.com/accessonly                   |800-709-7696   |
      |https://MSIaccess.premedicareplans.com/accessonly             |866-867-6897   |
      |https://ComcastNBCUAccess.premedicareplans.com/accessonly     |855-246-4074   |
      |https://OfficeDepot.premedicareplans.com/accessonly           |866-435-5230   |

  @Regression @VerifyGatedPortalFlow
  Scenario Outline: Verify the Gated portal flow
    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User able to verify text on the homepage
    And   User able to verify the Here what to expect text at the footer
    And   User able to click on the get started button
    And   User Try login to the portal with wrong credentials and error msg is displayed having "<TollFreeNumber>"
    And   User enter correct credentials with "<FirstName>","<LastName>"and"<SSN>"to login successfully
    And   User verifies shop for health insurance page
    And   User verify end date for Open enrollment
    And   User able to select any option on the page
    And   User able to enter household details and household income
    Then  User Click on See Your Options to Lower Your Cost button and Compare option is displayed
    Examples:
      | GetInsuredURL                                                |TollFreeNumber |FirstName |LastName|SSN |
      |https://RetireeHealth.premedicareplans.com/subsidy            |866-609-4810   |TESTAP    |PRODA   |1001|
      |https://Ahlstrom.premedicareplans.com/subsidy                 |844-618-6285   |TESTAHLS  |PRODB   |1002|
      |https://Momentive.premedicareplans.com/subsidy                |888-264-9323   |TESTMMT   |PRODD   |1004|
      |https://Motorolasolutions.premedicareplans.com/subsidy        |844-851-5426   |TESTMSI   |PRODE   |1005|
      |https://TransCanada.premedicareplans.com/subsidy              |844-618-6289   |TESTTC    |PRODF   |1006|
      |https://ComcastNBCU.premedicareplans.com/subsidy              |866-435-5135   |TESTCCDPHL|PRODG   |1007|

  @Regression @VerifyUnGatedPortalFlow
  Scenario Outline: Verify the Ungated portal flow
    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User clicks on the start shopping button
    And   Health insurance page should be displayed
    And   User selects any option on the health insurance Page
    And   Household page should be displayed
    And   User enter the household details and its income
    #And   User details should be updated successfully
    #And   User clicks on See your plans button
    Then  User should be redirect to On Exchange flow

    Examples:
    #UnGated Portal
      | GetInsuredURL                                               |
      |https://RetireeHealthAccess.premedicareplans.com/accessonly   |
      |https://AECOM.premedicareplans.com/accessonly                 |
      |https://Albemarle.premedicareplans.com/accessonly             |
      |https://Devon.premedicareplans.com/accessonly                 |
      |https://Retiree.premedicareplans.com/accessonly               |
      |https://Quest.premedicareplans.com/accessonly                 |
      |https://Silgan.premedicareplans.com/accessonly                |
      |https://TCaccess.premedicareplans.com/accessonly              |
      |https://URS.premedicareplans.com/accessonly                   |
      |https://MSIaccess.premedicareplans.com/accessonly             |
      |https://ComcastNBCUAccess.premedicareplans.com/accessonly     |
      |https://OfficeDepot.premedicareplans.com/accessonly           |

  @Regression @VerifyProductsLinkGatedPortal
  Scenario Outline: To Verify product links for gated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    When  User able to click on the get started button
    And   User enter correct credentials with "<FirstName>","<LastName>"and"<SSN>"to login successfully
    And   User verifies the Health Product link
    And   User verifies the Medicare Product link
    And   User verifies the dental product link
    And   User verifies the Vision product link
    Then  User verifies the other insurance product link appear only for "<GetInsuredURL>" Motorola Client

    Examples:
      | GetInsuredURL                                           |FirstName |LastName|SSN |
      |https://RetireeHealth.premedicareplans.com/subsidy       |TESTAP    |PRODA   |1001|
      |https://Ahlstrom.premedicareplans.com/subsidy            |TESTAHLS  |PRODB   |1002|
      |https://Momentive.premedicareplans.com/subsidy           |TESTMMT   |PRODD   |1004|
      |https://Motorolasolutions.premedicareplans.com/subsidy   |TESTMSI   |PRODE   |1005|
      |https://TransCanada.premedicareplans.com/subsidy         |TESTTC    |PRODF   |1006|
      |https://ComcastNBCU.premedicareplans.com/subsidy         |TESTCCDPHL|PRODG   |1007|


  @Regression @VerifyProductsLinkUnGatedPortal
  Scenario Outline: To Verify product links for ungated portal
    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User clicks on the start shopping button
    And   Health insurance page should be displayed
    And   User verifies the Health Product link
    And   User verifies the Medicare Product link
    And   User verifies the dental product link
    And   User verifies the Vision product link
    Examples:
    #UnGated Portal
      | GetInsuredURL                                               |
      |https://RetireeHealthAccess.premedicareplans.com/accessonly   |
      |https://AECOM.premedicareplans.com/accessonly                 |
      |https://Albemarle.premedicareplans.com/accessonly             |
      |https://Devon.premedicareplans.com/accessonly                 |
      |https://Retiree.premedicareplans.com/accessonly               |
      |https://Quest.premedicareplans.com/accessonly                 |
      |https://Silgan.premedicareplans.com/accessonly                |
      |https://TCaccess.premedicareplans.com/accessonly              |
      |https://URS.premedicareplans.com/accessonly                   |
      |https://MSIaccess.premedicareplans.com/accessonly             |
      |https://ComcastNBCUAccess.premedicareplans.com/accessonly     |
      |https://OfficeDepot.premedicareplans.com/accessonly           |

  @Regression @VerifyHouseholdScreenerGatedPortal
  Scenario Outline: To Verify Household screener for gated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    When  User able to click on the get started button
    And   User enter correct credentials with "<FirstName>","<LastName>"and"<SSN>"to login successfully
    And   User able to select any option on the page
    Then  User enter wrong zipcode and error msg should be displayed
    And   User make new household members with wrong birthdate and should be accepted
    And   User clicks on See Your Options and error msg should be displayed
    And   User try to add children below 26 years age and should be added
    And   Tobacco checkbox should be present and be checked
    And   User able to check seeking Coverage checkbox should be checked
    And   User able to see pregnant checkbox should be present



    Examples:
      | GetInsuredURL                                           |FirstName |LastName|SSN |
      |https://RetireeHealth.premedicareplans.com/subsidy       |TESTAP    |PRODA   |1001|
      |https://Ahlstrom.premedicareplans.com/subsidy            |TESTAHLS  |PRODB   |1002|
      |https://Momentive.premedicareplans.com/subsidy           |TESTMMT   |PRODD   |1004|
      |https://Motorolasolutions.premedicareplans.com/subsidy   |TESTMSI   |PRODE   |1005|
      |https://TransCanada.premedicareplans.com/subsidy         |TESTTC    |PRODF   |1006|
      |https://ComcastNBCU.premedicareplans.com/subsidy         |TESTCCDPHL|PRODG   |1007|


  @Regression @VerifyHouseholdScreenerUnGatedPortal
  Scenario Outline: To Verify Household screener for gated portal
    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User clicks on the start shopping button
    And   Health insurance page should be displayed
    And   User selects any option on the health insurance Page
    Then  User enter wrong zipcode and error msg should be displayed
    And   User make new household members with wrong birthdate and should be accepted
    And   User clicks on Continue and error msg should be displayed
    And   User try to add children below 26 years age and should be added
    And   Tobacco checkbox should be present and be checked
    And   User able to check seeking Coverage checkbox should be checked
    And   User able to see pregnant checkbox should be present
    Examples:
        #UnGated Portal
      | GetInsuredURL                                               |
      |https://RetireeHealthAccess.premedicareplans.com/accessonly   |
      |https://AECOM.premedicareplans.com/accessonly                 |
      |https://Albemarle.premedicareplans.com/accessonly             |
      |https://Devon.premedicareplans.com/accessonly                 |
      |https://Retiree.premedicareplans.com/accessonly               |
      |https://Quest.premedicareplans.com/accessonly                 |
      |https://Silgan.premedicareplans.com/accessonly                |
      |https://TCaccess.premedicareplans.com/accessonly              |
      |https://URS.premedicareplans.com/accessonly                   |
      |https://MSIaccess.premedicareplans.com/accessonly             |
      |https://ComcastNBCUAccess.premedicareplans.com/accessonly     |
      |https://OfficeDepot.premedicareplans.com/accessonly           |

  @Regression @HRAInformationVerificationGatedPortal
  Scenario Outline: To Verify HRA Information for gated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    When  User able to click on the get started button
    And   User enter correct credentials with "<FirstName>","<LastName>"and"<SSN>"to login successfully
    And   User able to select any option on the page
    And   User clicks on See Your Options and compare your option page should be displayed
    #Then  User verifies the HRA decision Page
    And   User verifies Decision Page Footer
    And   User clicks on Shop with employer contribution and navigate to OFF Exchange Flow
    And   Personalized plan score page should be displayed
    And   User close the Personalised plan score and clicks on Learn More in Health Plan Page
    And   User able to verify the tooltip text for Learn More Link


    Examples:
      | GetInsuredURL                                           |FirstName |LastName|SSN |
      |https://RetireeHealth.premedicareplans.com/subsidy       |TESTAP    |PRODA   |1001|
      |https://Ahlstrom.premedicareplans.com/subsidy            |TESTAHLS  |PRODB   |1002|
      |https://Momentive.premedicareplans.com/subsidy           |TESTMMT   |PRODD   |1004|
      |https://Motorolasolutions.premedicareplans.com/subsidy   |TESTMSI   |PRODE   |1005|
      |https://TransCanada.premedicareplans.com/subsidy         |TESTTC    |PRODF   |1006|
      |https://ComcastNBCU.premedicareplans.com/subsidy         |TESTCCDPHL|PRODG   |1007|


  @Regression @HealthPlanVerificationUnGatedPortal
  Scenario Outline: To Verify Health Plan for Ungated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    When  User clicks on the start shopping button
    And   Health insurance page should be displayed
    And   User able to select any option on the page
    And   User fills household data and click on the continue Button
    And   User able to open Personalised Plan Page
    And   User should select three plans for comparision
    Then  User clicks on Compare plan and comparision page should be displayed
    And   User verifies Verify three plans header is displayed
    And   User clicks on back to all plans and page should be displayed
    And   User clicks on view details for one plan and can see it successfully
    And   User opens the personalised plan scores page
    And   User types a doctors name and it should get selected
    And   User can select drug with dosage successfully
    And   User can able to select optional benefit and it should be checked
    And   User clicks on Personalised plan score and it should be displayed
    Examples:
           #UnGated Portal
      | GetInsuredURL                                               |
      |https://RetireeHealthAccess.premedicareplans.com/accessonly   |
      |https://AECOM.premedicareplans.com/accessonly                 |
      |https://Albemarle.premedicareplans.com/accessonly             |
      |https://Devon.premedicareplans.com/accessonly                 |
      |https://Retiree.premedicareplans.com/accessonly               |
      |https://Quest.premedicareplans.com/accessonly                 |
      |https://Silgan.premedicareplans.com/accessonly                |
      |https://TCaccess.premedicareplans.com/accessonly              |
      |https://URS.premedicareplans.com/accessonly                   |
      |https://MSIaccess.premedicareplans.com/accessonly             |
      |https://ComcastNBCUAccess.premedicareplans.com/accessonly     |
      |https://OfficeDepot.premedicareplans.com/accessonly           |

  @Regression @HealthPlanVerificationGatedPortal
  Scenario Outline: To Verify Health Plan for gated portal
    Given User opens the Get Insured URL "<GetInsuredURL>" for regression
    When  User able to click on the get started button
    And   User enter correct credentials with "<FirstName>","<LastName>"and"<SSN>"to login successfully
    And   User able to select any option on the page
    And   User clicks on See Your Options and compare your option page should be displayed
    And   User clicks on Shop with tax credits and personalized page is displayed
    And   User should select three plans for comparision
    Then  User clicks on Compare plan and comparision page should be displayed
    And   User verifies Verify three plans header is displayed
    And   User clicks on back to all plans and page should be displayed
    And   User clicks on view details for one plan and can see it successfully
    And   User opens the personalised plan scores page
    And   User types a doctors name and it should get selected
    And   User can select drug with dosage successfully
    And   User can able to select optional benefit and it should be checked
    And   User clicks on Personalised plan score and it should be displayed for GatedPortal

    Examples:
      | GetInsuredURL                                           |FirstName |LastName|SSN |
     ## |https://RetireeHealth.premedicareplans.com/subsidy       |TESTAP    |PRODA   |1001|
      |https://Ahlstrom.premedicareplans.com/subsidy            |TESTAHLS  |PRODB   |1002|
      #|https://Momentive.premedicareplans.com/subsidy           |TESTMMT   |PRODD   |1004|
     # |https://Motorolasolutions.premedicareplans.com/subsidy   |TESTMSI   |PRODE   |1005|
      ##|https://TransCanada.premedicareplans.com/subsidy         |TESTTC    |PRODF   |1006|
     # |https://ComcastNBCU.premedicareplans.com/subsidy         |TESTCCDPHL|PRODG   |1007|